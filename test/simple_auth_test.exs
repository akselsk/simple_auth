defmodule SimpleAuthTest do
  use ExUnit.Case
  doctest SimpleAuth
  alias SimpleAuth

  test "happy path" do
      assert SimpleAuth.validate_email("lol@msn.com") == {:ok, "lol@msn.com"}
      assert SimpleAuth.validate_pwd("sammadet") == {:ok, "sammadet"}
      assert SimpleAuth.hash_pwd("sammadet")
      |> SimpleAuth.verify_pass("sammadet") == {:ok, "authenticated"}
  end

  test "error_path" do
      assert SimpleAuth.validate_email("lolmsn.com") == {:error, "Must contain @"}
      assert SimpleAuth.validate_pwd("12") == {:error, "Password must be more than 8 characters"}
      assert SimpleAuth.validate_email_and_pwd({"lolmsn","12"}) == {:error, "Must contain @"}
      assert SimpleAuth.validate_email_and_pwd({"lol@msn.com","12"}) == {:error, "Password must be more than 8 characters"}
      end
end
