defmodule SimpleAuth do
  @moduledoc """
  Helper functions for authenticaiton
  """

# TODO make this a bit stricter
@doc """
	Validats the password. Must be longer than x characters
	"""
  def validate_pwd(pwd) do
    case pwd do
      nil ->
        {:error, "Password cant be nil"}
      x ->
        if String.length(x) > 3 do
          {:ok, pwd}
        else
          {:error, "Password must be more than 8 characters"}
        end
    end
  end

def hash_pwd(pwd), do: Argon2.hash_pwd_salt(pwd)

  def validate_email(email) do
    case email do
      nil ->
        {:error, "Can not be nil"}
      x ->
        if String.contains?(x, "@") do
          {:ok, email}
        else
          {:error, "Must contain @"}
        end
    end
  end

  def validate_email_and_pwd({email,pwd}) do
      validate_email(email)
      |> case do
        {:error, reason} -> {:error, reason}
        _ -> validate_pwd(pwd)
            |> case do
              {:error,reason} -> {:error,reason}
              x -> x
             end
       end
  end

  def verify_pass(hash, password) do
    Argon2.verify_pass(password, hash)
    |> case do
      true -> {:ok, "authenticated"}
      false -> {:error, "wrong username/ password"}
    end
  end


end
